#!/usr/bin/env python
import sys, os, time, atexit
from signal import SIGTERM 
import optparse

from annotator.app import app, setup_app


class Daemon:
    """
    A generic daemon class.
    
    Usage: subclass the Daemon class and override the run() method
    """
    def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
    
    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced 
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try: 
                pid = os.fork() 
                if pid > 0:
                        # exit first parent
                        sys.exit(0) 
        except OSError, e: 
                sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
                sys.exit(1)

        # decouple from parent environment
        os.chdir("/") 
        os.setsid() 
        os.umask(0) 

        # do second fork
        try: 
                pid = os.fork() 
                if pid > 0:
                        # exit from second parent
                        sys.exit(0) 
        except OSError, e: 
                sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
                sys.exit(1) 

        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = file(self.stdin, 'r')
        so = file(self.stdout, 'a+')
        se = file(self.stderr, 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

        # write pidfile
        atexit.register(self.delpid)
        pid = str(os.getpid())
        file(self.pidfile,'w+').write("%s\n" % pid)
    
    def delpid(self):
        os.remove(self.pidfile)

    def start(self):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        try:
                pf = file(self.pidfile,'r')
                pid = int(pf.read().strip())
                pf.close()
        except IOError:
                pid = None

        if pid:
                message = "pidfile %s already exist. Daemon already running?\n"
                sys.stderr.write(message % self.pidfile)
                sys.exit(1)
        
        # Start the daemon
        self.daemonize()
        self.run()

    def stop(self):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        try:
                pf = file(self.pidfile,'r')
                pid = int(pf.read().strip())
                pf.close()
        except IOError:
                pid = None

        if not pid:
                message = "pidfile %s does not exist. Daemon not running?\n"
                sys.stderr.write(message % self.pidfile)
                return # not an error in a restart

        # Try killing the daemon process	
        try:
                while 1:
                        os.kill(pid, SIGTERM)
                        time.sleep(0.1)
        except OSError, err:
                err = str(err)
                if err.find("No such process") > 0:
                        if os.path.exists(self.pidfile):
                                os.remove(self.pidfile)
                else:
                        print str(err)
                        sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def run(self):
        """
        You should override this method when you subclass Daemon. It will be called after the process has been
        daemonized by start() or restart().
        """


class StoreDaemon(Daemon):
    def run(self):
        ip = self.options.host
        port = self.options.port
        log = self.options.log
        setup_app(log) 
        app.run(host=ip, port=port)


def check_options(options):
    if options.log == None:
        parser.error('Please supply the -l / --log option')


if __name__ == '__main__':

    # set up the option parser
    parser = optparse.OptionParser()
    parser.add_option("-l", "--log", dest='log',
            help="path of logfile to be used")
    parser.add_option("-i", "--ip", dest='host',
            help="IP to bind annotator store to", default='0.0.0.0')
    parser.add_option("-p", "--port", dest='port',
            help="port to use", type="int",  default='5500')
    parser.add_option("--pid", dest="pid", help="path of pid file")
    parser.add_option("--start", action="store_true", dest="start_daemon", help="start deamon")
    parser.add_option("--stop", action="store_true", dest="stop_daemon", help="stop deamon")

    (options, args) = parser.parse_args(sys.argv)
   
    if options.start_daemon or options.stop_daemon:
            if options.pid == None:
                parser.error('Please supply the --pid option')

            if options.start_daemon:
                daemon = StoreDaemon(options.pid)
                daemon.options = options
                check_options(options)
                daemon.start()
            elif options.stop_daemon:
                daemon = StoreDaemon(options.pid)
                daemon.stop()
    else:
        # If the user did not ask us to daemonize, just run like in the old
        # days
        setup_app(None)
        app.run(host=options.host, port=options.port)



