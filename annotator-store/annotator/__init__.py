"""Backend for web annotation.

@copyright: (c) 2006-2010 Open Knowledge Foundation
"""
__version__ = '0.5'
__license__ = 'MIT'
__author__ = 'Rufus Pollock and Nick Stenning (Open Knowledge Foundation)'
