from flask import Flask, Module, Response
from flask import abort, json, redirect, request, url_for, g
from .model import Annotation
from authz import authorize, ACTION
from . import auth

#import roundup

__all__ = ["store"]

store = Module(__name__)

from flask import current_app 


# We define our own jsonify rather than using flask.jsonify because we wish
# to jsonify arbitrary objects (e.g. index returns a list) rather than kwargs.
def jsonify(obj, *args, **kwargs):
    res = json.dumps(obj, indent=None if request.is_xhr else 2)
    return Response(res, mimetype='application/json', *args, **kwargs)

def unjsonify(str):
    return json.loads(str)

def get_current_userid():
    return auth.get_request_userid(request)

@store.before_request
def before_request():
    g.account_id = request.headers.get('x-annotator-account-id', '')
    if current_app.config['AUTH_ON'] and not request.method == 'GET' and not auth.verify_request(request):
        return jsonify("Cannot authorise request. Perhaps you didn't send the x-annotator headers?", status=401)

@store.after_request
def after_request(response):
    response.headers['Access-Control-Allow-Origin']   = '*'
    # response.headers['Access-Control-Allow-Headers'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'X-Requested-With, Content-Type, X-Annotator-Account-Id, X-Annotator-User-Id, X-Annotator-Auth-Token-Valid-Until, X-Annotator-Auth-Token'
    response.headers['Access-Control-Expose-Headers'] = 'Location'
    response.headers['Access-Control-Allow-Methods']  = 'GET, POST, PUT, DELETE'
    response.headers['Access-Control-Max-Age']        = '86400'

    return response

@store.route('/')
def home():
    return jsonify('Annotator Store API')

# INDEX
@store.route('/annotations')
def index():
    annotations = [anno.to_dict() for anno in Annotation.search() if authorize(anno, 'read', get_current_userid())]
    return jsonify(annotations)

# CREATE
@store.route('/annotations', methods=['POST'])
def create_annotation():
    if request.json:
        annotation = Annotation.from_dict(request.json)
        if g.account_id:
            annotation.account_id = g.account_id
        annotation.save()
        #TODO Must check if roundup has been activated in the .cfg file
        #roundup.create_issue(annotation.to_dict())
        return jsonify(annotation.to_dict())
    else:
        return jsonify('No parameters given. Annotation not created.', status=400)

# READ
@store.route('/annotations/<id>')
def read_annotation(id):
    #print id
    annotation = Annotation.get(id)
     

    if not annotation:
        return jsonify('Annotation not found.', status=404)
    elif authorize(annotation, 'read', get_current_userid()):
        return jsonify(annotation.to_dict())
    else:
        return jsonify('Could not authorise request. Read not allowed', status=401)

# UPDATE
@store.route('/annotations/<id>', methods=['POST', 'PUT'])
def update_annotation(id):
    annotation = Annotation.get(id)

    if not annotation:
        return jsonify('Annotation not found. No update performed.', status=404)

    elif request.json and authorize(annotation, 'update', get_current_userid()):
        updated = Annotation.from_dict(request.json)
        if updated.permissions != annotation.permissions:
            if not authorize(annotation, ACTION.ADMIN, get_current_userid()):
                return jsonify('Could not authorise request (permissions change). No update performed', status=401)
        updated.save()
        #roundup.update_issue(updated.to_dict()) 
        return jsonify(updated.to_dict())
    else:
        return jsonify('Could not authorise request. No update performed', status=401)

# DELETE
@store.route('/annotations/<id>', methods=['DELETE'])
def delete_annotation(id):
    annotation = Annotation.get(id)

    if not annotation:
        return jsonify('Annotation not found. No delete performed.', status=404)

    elif authorize(annotation, 'delete', get_current_userid()):
        #roundup.delete_issue(annotation.to_dict())
        annotation.delete()
        return None, 204

    else:
        return jsonify('Could not authorise request. No update performed', status=401)

# Search
@store.route('/search')
def search_annotations():
    kwargs = dict(request.args.items())
    results = [ x.to_dict() for x in Annotation.search(**kwargs) ]
    #for r in results:
        #print r,'\n'
    total = Annotation.count(**kwargs)
    qrows = {
        'total': total,
        'rows': results,
    }
    return jsonify(qrows)
