import xmlrpclib
import zlib
import re, htmlentitydefs
import couchdb

DB = 'annotator-test'   # Change this to whatever it needs to be
SERVER = 'http://localhost:5984/'

server = couchdb.Server(SERVER)
annotate_db = server[DB]



rounduptracker = 'http://admin:g00gl3sucks!@localhost:8080/siyavulatracker/'
roundup_server = xmlrpclib.ServerProxy(rounduptracker, allow_none=True)

# from: http://effbot.org/zone/re-sub.htm#unescape-html
##
# Removes HTML or XML character references and entities from a text string.
#
# @param text The HTML (or XML) source text.
# @return The plain text, as a Unicode string, if necessary.

def unescape(text):
    #print type(text)
    if not type(text) in ['unicode', 'str']:
        return text
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return unichr(int(text[3:-1], 16))
                else:
                    return unichr(int(text[2:-1]))
            except ValueError:
                pass
        else:
            # named entity
            try:
                text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
            except KeyError:
                pass
        return text # leave as is
    return re.sub("&#?\w+;", fixup, text)





def format_message(message):
    '''Formats the message properly'''
     # make the message a width of 66 characters.
    temp =[m for m in message]
    for i,m in enumerate(temp):
        # split the line at 66 characters
        if i % 66 == 0:
            # but only if there's a space
            if m == ' ':
                temp[i]= '\n'
            # else break it at the previous space.
            else:
                k = i - 1
                if k < 0: break
                while True:
                    if temp[k] == ' ':
                        temp[k]= '\n'
                        break
                    else:
                        k -= 1
    return ''.join(temp)



def create_message(annotation, ann_id):
    '''Create a message that will go to the bugtracker
    ann_id is the id of the annotation on roundup, NOT annotator
    '''
    # use the first 10 words for a title
    title = ' '.join(annotation['text'].split()[0:10])
    # make a string from the annotation data
    
    for k in annotation.keys():
        s = unescape(annotation[k])
        #print s
#        print u' '.join([unicode(k), unicode(s)])
    
    # message that will be sent to the bugtracker
    message = \
'''
By: %s
--------------
Quoted text
--------------
%s


--------------
Comment
--------------
%s


--------------
ann%s
''' \
%(annotation['user']['name'], format_message(annotation['quote']), format_message(annotation['text']), ann_id)


    return message


def roundup_setup():
    ''' Get the configuration from the .cfg file'''
    cfg = open('annotator.cfg', 'r').readlines()
    cfg = [c for c in cfg if 'ROUNDUP' in c]

    USE_ROUNDUP = [c for c in cfg if 'USE_ROUNDUP' in c][0].split()[2]
    if USE_ROUNDUP.lower() == 'true':
        ROUNDUP_HOST = [c for c in cfg if 'ROUNDUP_HOST' in c][0].split()[2]
    
        
def create_issue(annotation):
    '''\n\n Create a new roundup issue when a new annotation is created '''
   
    try:
        content = u'\n'.join([''.join([unicode(unescape(key)), ' : ', unicode(unescape(annotation[key]))]) for key in annotation.keys()]) 
 
        ann = roundup_server.create('ann',
                'content=%s'%content,
                'category=%s'%annotation['category'],
                'account_id=%s'% annotation['account_id'],
                'created=%s'% annotation['created'],
                'text=%s'% annotation['text'],
                '_rev=%s'% annotation['_rev'],
                'uri=%s'% annotation['uri'],
                'ranges=%s'% annotation['ranges'],
                'annotator_schema_version=%s'% annotation['annotator_schema_version'],
                'user=%s'% annotation['user'],
                'quote=%s'% annotation['quote'],
                '_id=%s'% annotation['id'],
                'type=%s'% annotation['type'],
                'permissions=%s'% annotation['permissions'])
        message = create_message(annotation, ann)
        title = ' '.join(annotation['text'].split()[0:10])
        msg = roundup_server.create('msg', 'content=%s'%message, 'messageid=%s'%annotation['id'], 'author=%s'%'admin')
        issue = roundup_server.create('issue', 'title="%s"'%title, 'messages=%s'%msg, 'ann=%s'%ann )

        roundup_server.set('msg%s'%msg, 'summary=%s'%issue)

        # add the issue id to the annotation
        annsaved = annotate_db[annotation['_id']]
        annsaved['issue_id'] = issue
        annotate_db.save(annsaved)
        print annsaved.keys()
        
        


    except Exception as e:
        print '''Something went wrong while trying to send annotation to roundup. Here is the exception returned:\n%s'''%e






def update_issue(updated):
    '''Makes an update to the issue'''


    try:

        # if a comment is updated, it will have the same id as the original one, so we must find 
        # the issue containing that id.
        

        ann_id = updated['id']

        # find the message on Roundup with that id
        msg_no = roundup_server.filter('msg', None, {'messageid' :'%s'%ann_id})[0]
        
        # get the issue ID
        issue_id = roundup_server.display('msg%s'%msg_no, 'summary')['summary']
        content = u'\n'.join([''.join([unicode(unescape(key)), ' : ', unicode(unescape(updated[key]))]) for key in updated.keys()]) 
        # create a new annotation instance on roundup
        ann = roundup_server.create('ann',
                    'content=%s'%content,
                    'category=%s'%updated['category'],
                    'account_id=%s'% updated['account_id'],
                    'created=%s'% updated['created'],
                    'text=%s'% updated['text'],
                    '_rev=%s'% updated['_rev'],
                    'uri=%s'% updated['uri'],
                    'ranges=%s'% updated['ranges'],
                    'annotator_schema_version=%s'% updated['annotator_schema_version'],
                    'user=%s'% updated['user'],
                    'quote=%s'% updated['quote'],
                    '_id=%s'% updated['id'],
                    'type=%s'% updated['type'],
                    'permissions=%s'% updated['permissions'])

        # now create an updated message and add it to that issue
        message = create_message(updated, ann)
        msg = roundup_server.create('msg', 'content=%s'%message, 'messageid=%s'%updated['id'], 'author=%s'%'admin')
        # update the issue by adding the msg id to its list of messages.
        # first get the messages of that issue.
        messages = roundup_server.display('issue%s'%issue_id, 'messages')['messages']
        print messages
        
        messages.append(msg)


        # update the issue
        roundup_server.set('issue%s'%issue_id, 'messages=%s'%','.join(messages))
    except Exception as e:
        print '''Something went wrong while trying to update and send annotation to roundup. Here is the exception returned:\n%s'''%e

    

def delete_issue(annotation):
    '''\n\n post a message on Roundup that the annotation was deleted'''
   
    try: 
        message = "This annotation was deleted by %s" % annotation['user']['name']

        ann_id = annotation['id']

        # find the message on Roundup with that id
        msg_no = roundup_server.filter('msg', None, {'messageid' :'%s'%ann_id})[0]
        
        # get the issue ID
        issue_id = roundup_server.display('msg%s'%msg_no, 'summary')['summary']

        # now create an updated message and add it to that issue
        msg = roundup_server.create('msg', 'content=%s'%message, 'messageid=%s'%annotation['id'], 'author=%s'%'admin')
        
        # update the issue by adding the msg id to its list of messages.
        # first get the messages of that issue.
        messages = roundup_server.display('issue%s'%issue_id, 'messages')['messages']
        messages.append(msg)

        # update the issue
        roundup_server.set('issue%s'%issue_id, 'messages=%s'%','.join(messages))
    except Exception as e:
        print '''Something went wrong while trying to delete annotation. Here is the exception returned:\n%s'''%e
 




roundup_setup()
